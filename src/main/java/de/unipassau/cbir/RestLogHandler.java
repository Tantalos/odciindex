package de.unipassau.cbir;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class RestLogHandler extends Handler {
	
	private String requestMethod = "POST";
	private HttpURLConnection logEndpoint;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS'Z'");
	private final String target;
	
	public RestLogHandler(String target) throws IOException {
		this.target = target;
	}


	@Override
	public void publish(LogRecord record) {
		try {
		URL url = new URL(target);
		logEndpoint = (HttpURLConnection) url.openConnection();
        logEndpoint.setDoOutput(true);
		logEndpoint.setRequestMethod(requestMethod);
		logEndpoint.setRequestProperty("Accept", "application/json");
		
		Date date = new Date(record.getMillis());
		StringBuilder log = new StringBuilder();
		log.append(dateFormat.format(date)).append("  ");
		log.append(record.getLevel()).append(" ");
		log.append(record.getSequenceNumber()).append(" ---");
		log.append(" [").append(record.getThreadID()).append("] ");
		log.append(record.getSourceClassName()).append(" : ");
		log.append(record.getMessage());

		OutputStream outputStream = logEndpoint.getOutputStream();
			outputStream.write(log.toString().getBytes(Charset.forName("UTF-8")));
	        outputStream.flush();
	        
			if (logEndpoint.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ logEndpoint.getResponseCode());
					            }

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	


	@Override
	public void flush() {
	}

	@Override
	public void close() throws SecurityException {
	}
}
