
begin
    DBMS_OUTPUT.put_line('java util logging');
dbms_java.grant_permission( 'SYSTEM', 'SYS:java.util.logging.LoggingPermission', 'control', '' );
end;
/
		

begin
    DBMS_OUTPUT.put_line('enable remote connection');
dbms_java.grant_permission( 'SYSTEM', 'SYS:java.net.SocketPermission', '10.0.2.2:8090', 'connect,resolve' );
end;
/
			
    
begin
    DBMS_OUTPUT.put_line('enable runtime permission	accessDeclaredMembers');
dbms_java.grant_permission( 'SYSTEM', 'SYS:java.lang.RuntimePermission', 'accessDeclaredMembers', '' );
end;
/
			

begin
    DBMS_OUTPUT.put_line('enable reflections for jackson');
dbms_java.grant_permission( 'SYSTEM', 'SYS:java.lang.reflect.ReflectPermission', 'suppressAccessChecks', '' );
end;
/