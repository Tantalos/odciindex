package de.unipassau.cbir;

import static de.unipassau.cbir.LogUtil.EtoStr;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Logger;

import oracle.CartridgeServices.ContextManager;
import oracle.CartridgeServices.CountException;
import oracle.CartridgeServices.InvalidKeyException;
import oracle.ODCI.ODCIColInfo;
import oracle.ODCI.ODCIEnv;
import oracle.ODCI.ODCIIndexInfo;
import oracle.ODCI.ODCIPredInfo;
import oracle.ODCI.ODCIQueryInfo;
import oracle.ODCI.ODCIRidList;
import oracle.jdbc.driver.OracleConnection;
import oracle.sql.BFILE;
import oracle.sql.CustomDatum;
import oracle.sql.CustomDatumFactory;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

public class ODCIIndex implements CustomDatum, CustomDatumFactory {

    private final static String HOST = "http://localhost:8090";
    private final static LireApi LIRE = new LireApi(HOST); 
    private final static Logger log = LogUtil.getRestLogger(ODCIIndex.class.getName());
    
    
    private int key;

    public ODCIIndex(int key) {
    	this.key = key;
    }

    public static BigDecimal ODCIIndexCreate(ODCIIndexInfo ia, String parms, ODCIEnv env) throws SQLException {
		try {
			String indexName = ia.getIndexName();
			boolean success = LIRE.createIndex(indexName);
			if(success) {
				log.info("Index " + indexName + " was created.");
			} else {
				log.severe("Error while creating index");
				return ODCIConst.ODCI_ERROR;
			}
			
			success &= indexTable(ia);
			if(success) {
				log.info("Elements of table were added to index " + indexName);
			} else {
				log.severe("Error while adding elements of table to index " + indexName);
				return ODCIConst.ODCI_ERROR;
			}
			
			return ODCIConst.ODCI_SUCCESS;
		} catch (IOException e) {
			log.severe(EtoStr(e));
			return ODCIConst.ODCI_ERROR;
		} catch (Throwable t) {
			log.severe(EtoStr(t));
			throw t;
		}
	}


    public static BigDecimal ODCIIndexDrop(ODCIIndexInfo ia, ODCIEnv env) throws SQLException {
		try {
			String indexName = ia.getIndexName();
			log.info("");
			
			if(LIRE.deleteIndex(indexName)) {
				log.info("index " + indexName + " was droped");
			} else {
				log.severe("Error while dropping index " + indexName);
				return ODCIConst.ODCI_ERROR;
			}
			return ODCIConst.ODCI_SUCCESS;
		} catch (IOException e) {
			log.severe(EtoStr(e));
			return ODCIConst.ODCI_ERROR;
		} catch (Throwable t) {
			log.severe(EtoStr(t));
			throw t;
		}
    }

    public static BigDecimal ODCIIndexInsert(ODCIIndexInfo ia, String rid, BFILE newval, ODCIEnv env) throws SQLException {
    	try {
			String indexName = ia.getIndexName();
			String entry = newval.getDirAlias() + newval.getName();
			
			if (LIRE.insertIndex(indexName, entry)) {
				log.info("inserted " + entry + " to index " + indexName);
			} else {
				log.severe("Error while inserting entry " + entry + " to index " +indexName );
				return ODCIConst.ODCI_ERROR;
			}
			return ODCIConst.ODCI_SUCCESS;
		} catch (IOException e) {
			log.severe(EtoStr(e));
			return ODCIConst.ODCI_ERROR;
		} catch (Throwable t) {
			log.severe(EtoStr(t));
			throw t;
		}
    }

    public static BigDecimal ODCIIndexDelete(ODCIIndexInfo ia, String rid, BFILE oldval, ODCIEnv env) throws SQLException {
		try {
			String indexName = ia.getIndexName();
			String entry = oldval.getDirAlias() + oldval.getName();

			if(LIRE.removeFromIndex(indexName, entry)) {
				log.info("deleted entry " + entry + " from index " + indexName);
			} else {
				log.severe("Error while deleting entry " + entry + " from index " + indexName);
				return ODCIConst.ODCI_ERROR;
			}

			return ODCIConst.ODCI_SUCCESS;
		} catch (IOException e) {
			log.severe(EtoStr(e));
			return ODCIConst.ODCI_ERROR;
		} catch (Throwable t) {
			log.severe(EtoStr(t));
			throw t;
		}
	}

    public static  BigDecimal ODCIIndexUpdate(ODCIIndexInfo ia, String rid, BFILE oldval, BFILE newval, ODCIEnv env)
            throws SQLException {
		try {
			String indexName = ia.getIndexName();
			String oldEntry = oldval.getDirAlias() + oldval.getName();
			String newEntry = newval.getDirAlias() + newval.getName();
			
			if(LIRE.update(indexName, oldEntry, newEntry)) {
				log.info("update old entry " + oldEntry + " in index " + indexName + " by the new entry " + newEntry);
			} else {
				log.severe("Error while updating old entry " + oldEntry + " in index " + indexName + " by the new entry " + newEntry);
				return ODCIConst.ODCI_ERROR;
			}
			
			return ODCIConst.ODCI_SUCCESS;
		} catch (IOException e) {
			log.severe(EtoStr(e));
			return ODCIConst.ODCI_ERROR;
		} catch (Throwable t) {
			log.severe(EtoStr(t));
			throw t;
		}
    }

	public static BigDecimal ODCIIndexStart(ODCIIndex[] sctx, ODCIIndexInfo ia, ODCIPredInfo pi, ODCIQueryInfo qi,
			BigDecimal strt, BigDecimal stop, String valargs, ODCIEnv env) throws SQLException {
		try {
			String sampleValue = valargs;
			String indexName = ia.getIndexName();
			
			List<String> results = LIRE.similarEntries(indexName, sampleValue);		
			if (results == null) {
				log.severe("Error occured while retrieving similar entries");
				return ODCIConst.ODCI_ERROR;
			}
			
			int max = results.size();
			log.info(max + " similar entries were retrieved");

			// XXX not demanded
//			if (results.isEmpty()) {
//				results = new ArrayList<>();
//			}
//
//			if (ODCIConst.ODCI_PRED_INCLUDE_START == pi.getFlags().intValue()) { // is
//																					// >=
//				int start = strt.intValue();
//				if (start >= max) {
//					results = new ArrayList<>();
//				}
//				results = results.subList(start, max);
//
//			} else if (ODCIConst.ODCI_PRED_INCLUDE_STOP == pi.getFlags().intValue()) { // is
//																						// <=
//				int stopp = stop.intValue();
//
//				if (stopp > max) {
//					stopp = max;
//				}
//				results = results.subList(0, stopp);
//			} else if (13 == pi.getFlags().intValue()) { // is =
//				int start = strt.intValue();
//				int stopp = stop.intValue();
//				assert start == stopp;
//
//				if (start == 0) {
//					results = results.subList(0, 1);
//				} else {
//					results = new ArrayList<>();
//				}
//			} else if (0 == pi.getFlags().intValue()) { // is < or >
//				if (strt != null) { // is >
//					int start = strt.intValue();
//					if (start >= max) {
//						results = new ArrayList<>();
//					}
//					results = results.subList(start + 1, max);
//				} else if (stop != null) { // is <
//					int stopp = stop.intValue();
//
//					if (stopp > max) {
//						stopp = max;
//					}
//					results = results.subList(0, stopp - 1);
//				} else {
//					return ODCIConst.ODCI_ERROR;
//				}
//			}

			ScanContext scanContext = new ScanContext(results, ia);

			int key = ContextManager.setContext(scanContext);
			sctx[0] = new ODCIIndex(key);
			
			return ODCIConst.ODCI_SUCCESS;
		} catch(IOException | CountException e) {
			log.severe(EtoStr(e));
			return ODCIConst.ODCI_ERROR;
		} catch (Throwable t) {
			log.severe(EtoStr(t));
			throw t;
		}
    }

    public BigDecimal ODCIIndexFetch(BigDecimal nrows, ODCIRidList[] rids, ODCIEnv env) throws SQLException {
		try {
			final int n = nrows.intValue();
			ScanContext scanContext = (ScanContext) ContextManager.getContext(key);
			List<String> nextResults = scanContext.getNextResults(n);
			final int resultCount = nextResults.size();
			log.info(resultCount + " elements were fetched");
			
			String[] ridsArray = getRowIdForFile(scanContext.getIa(), nextResults);
			rids[0] = new ODCIRidList(ridsArray);
			
			return ODCIConst.ODCI_SUCCESS;
		} catch(InvalidKeyException e) {
			log.severe(EtoStr(e));
			return ODCIConst.ODCI_ERROR;
		} catch (Throwable t) {
			log.severe(EtoStr(t));
			throw t;
		}
    }
    
    public BigDecimal ODCIIndexClose(ODCIEnv env) throws SQLException {
		try {
			ContextManager.clearContext(key);
			log.info("Similarity search was cosed");
			return ODCIConst.ODCI_SUCCESS;
		} catch(InvalidKeyException e) {
			log.severe(EtoStr(e));
			return ODCIConst.ODCI_ERROR;
		} catch (Throwable t) {
			log.severe(EtoStr(t));
			throw t;
		}
	}

	private String[] getRowIdForFile(ODCIIndexInfo ia, List<String> fileNames) throws SQLException {
    	Connection con = DriverManager.getConnection("jdbc:default:connection");
    	Statement stm  = con.createStatement();
		ODCIColInfo column = ia.getIndexCols().getArray()[0];
		String columnName = column.getColName();
		String tableName = "\"" + column.getTableSchema() + "\".\"" + column.getTableName() + "\"";
		int size = fileNames.size();
    	
		String[] rowIds = new String[size];
		int i = 0;
		for(String fileName : fileNames) {
	    	String sqlStatement = "SELECT ROWID FROM " + tableName + " WHERE getFileName(" + columnName + ") = '"+fileName+"' ";
	    	log.fine("Running SQLQuery: "+ sqlStatement);
	    	ResultSet result = stm.executeQuery(sqlStatement);
			
	    	//take only one rowID. Would be invoked again for duplicate entries
	    	if(result.next()) {
	    		rowIds[i] = result.getString(1);
	    	}
	    	i++;
		}
    	return rowIds;
    }
    
    private static boolean indexTable (ODCIIndexInfo ia) throws IOException, SQLException {
    	Connection con = DriverManager.getConnection("jdbc:default:connection");
    	Statement stm  = con.createStatement();
    	String indexName = ia.getIndexName();
		ODCIColInfo column = ia.getIndexCols().getArray()[0];
		String columnName = column.getColName();
		String tableName = "\"" + column.getTableSchema() + "\".\"" + column.getTableName() + "\"";
    	
    	String sqlStatement = "SELECT getFileName("+ columnName+") FROM " + tableName ;
    	
    	log.fine("Running SQLQuery: "+ sqlStatement);
    	ResultSet result = stm.executeQuery(sqlStatement);
		
    	boolean success = true;
    	while(result.next()) {
    		String entry = result.getString(1);
    		success &= LIRE.insertIndex(indexName, entry);
    		log.info("Already inserted table entry " + entry + " was added to index ");
    	}
    	return success;
    }

    /* ==================================================================
     *    Converter functions. Allows oracle to instantiate data object
     * ==================================================================
     */
	public static final ODCIIndex _employeeFactory = new ODCIIndex(1);
	private String SQL_NAME = "SYSTEM.IMAGEINDEXMETHODS"; // LIREINDEX

	public static CustomDatumFactory getFactory() {
		return _employeeFactory;
	}

	@Override
	public Datum toDatum(OracleConnection c) throws SQLException {
		log.fine("Passes object OUT. (Converts to oracle datum)");
		StructDescriptor sd = StructDescriptor.createDescriptor(SQL_NAME, c);

		Object[] attributes = {key};
		return new STRUCT(sd, c, attributes);
	}

	@Override
	public CustomDatum create(Datum d, int sqlType) throws SQLException {
		log.fine("Passes object IN. (Converts to custom datum)");
		if (d == null)
			return null;

		Object[] attributes = ((STRUCT) d).getAttributes();
		int key = ((BigDecimal) attributes[0]).intValue();
		return new ODCIIndex(key);
	}

}
