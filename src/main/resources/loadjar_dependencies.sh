 #!/bin/sh

if [[ -z "$1" ]]; then
    echo argument is missing. Please enter the path for your libraries
    exit 0;
fi

${ORACLE_HOME}/bin/loadjava -v -resolve $1/CartridgeServices.jar -user system/oracle
if [ $? -eq 0 ]; then
	echo CartridgeServices.jar loaded
else
    echo failed loading $1/CartridgeServices.jar
    exit 1;
fi


${ORACLE_HOME}/bin/loadjava -v -resolve $1/ODCI.jar -user system/oracle
if [ $? -eq 0 ]; then
	echo ODCI.jar loaded
else
    echo failed loading $1/ODCI.jar
    exit 1;
fi



${ORACLE_HOME}/bin/loadjava -v -resolve $1/jackson-core-2.9.5.jar -user system/oracle
if [ $? -eq 0 ]; then
	echo jackson-core-2.9.5.jar loaded
else
    echo failed loading $1/jackson-core-2.9.5.jar
    exit 1;
fi


${ORACLE_HOME}/bin/loadjava -v -resolve $1/jackson-annotations-2.9.5.jar -user system/oracle
if [ $? -eq 0 ]; then
	echo jackson-annotations-2.9.5.jar loaded
else
    echo failed loading $1/jackson-annotations-2.9.5.jar
    exit 1;
fi


${ORACLE_HOME}/bin/loadjava -v -resolve $1/jackson-databind-2.9.5.jar -user system/oracle
if [ $? -eq 0 ]; then
	echo jackson-databind-2.9.5.jar loaded
else
    echo failed loading $1/jackson-databind-2.9.5.jar
    exit 1;
fi


${ORACLE_HOME}/bin/loadjava -v -resolve $1/ODCIIndex-0.0.1-SNAPSHOT.jar -user system/oracle
if [ $? -eq 0 ]; then
	echo ODCIIndex-0.0.1-SNAPSHOT.jar loaded
else
    echo failed loading $1/ODCIIndex-0.0.1-SNAPSHOT.jar
    exit 1;
fi


echo finished...
