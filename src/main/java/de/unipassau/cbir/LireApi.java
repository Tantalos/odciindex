package de.unipassau.cbir;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

public class LireApi {

	private static final Logger log = LogUtil.getRestLogger(LireApi.class.getName());
	private static final ObjectMapper mapper = new ObjectMapper();

	private final String HOST;

	public LireApi(String host) {
			HOST = host;
	}


	public List<String> indexes() throws IOException, URISyntaxException {
		URI target = getUriFromString(HOST + "/index/");
		HttpURLConnection lireEndpoint = getConnection(target);
		lireEndpoint.setDoOutput(true);
		lireEndpoint.setRequestMethod("GET");
		lireEndpoint.setRequestProperty("Accept", "application/json");

		if(!call(lireEndpoint)) {
			return null;
		}
		String rawResult = read(lireEndpoint);
		@SuppressWarnings("unchecked")
		List<String> result = (List<String>) mapper.readValue(rawResult, List.class);
		return result;
	}

	public Boolean exists(String indexName) throws IOException {
		URI target = getUriFromString(HOST + "/index/" + indexName + "/");
		HttpURLConnection lireEndpoint = getConnection(target);
		lireEndpoint.setDoOutput(false);
		lireEndpoint.setRequestMethod("GET");
		lireEndpoint.setRequestProperty("Accept", "application/json");

		if(!call(lireEndpoint)) {
			return null;
		}
		String rawResult = read(lireEndpoint);
		Boolean result = mapper.readValue(rawResult, Boolean.class);
		
		return result;
	}

	public boolean createIndex(String indexName) throws IOException {
		URI target = getUriFromString(HOST + "/index/" + indexName + "/");
		HttpURLConnection lireEndpoint = getConnection(target);
		lireEndpoint.setDoOutput(false);
		lireEndpoint.setRequestMethod("POST");
		lireEndpoint.setRequestProperty("Accept", "application/json");

		return call(lireEndpoint);
	}

	public boolean deleteIndex(String indexName) throws IOException {
		URI target = getUriFromString(HOST + "/index/" + indexName + "/");
		HttpURLConnection lireEndpoint = getConnection(target);
		lireEndpoint.setDoOutput(false);
		lireEndpoint.setRequestMethod("DELETE");
		lireEndpoint.setRequestProperty("Accept", "application/json");

		return call(lireEndpoint);
	}

	public boolean truncateIndex(String indexName) throws IOException {
		URI target = getUriFromString(HOST + "/index/" + indexName + "/" + "truncate/");
		HttpURLConnection lireEndpoint = getConnection(target);
		lireEndpoint.setDoOutput(false);
		lireEndpoint.setRequestMethod("GET");
		lireEndpoint.setRequestProperty("Accept", "application/json");

		return call(lireEndpoint);
	}

	public boolean insertIndex(String indexName, String entry) throws IOException {
		// replacement of leading '/'. Must be done because of security violation in assignment of task in milestone 2
		entry = entry.replaceFirst("^\\/", "+");
		
		URI target = getUriFromString(HOST + "/index/" + indexName + "/" + entry + "/");
		HttpURLConnection lireEndpoint = getConnection(target);
		lireEndpoint.setDoOutput(false);
		lireEndpoint.setRequestMethod("POST");
		lireEndpoint.setRequestProperty("Accept", "application/json");

		return call(lireEndpoint);
	}

	public boolean removeFromIndex(String indexName, String entry) throws IOException {
		// replacement of leading '/'. Must be done because of security violation in assignment of task in milestone 2
		entry = entry.replaceFirst("^\\/", "+");
		
		URI target = getUriFromString(HOST + "/index/" + indexName + "/" + entry + "/");
		HttpURLConnection lireEndpoint = getConnection(target);
		lireEndpoint.setDoOutput(false);
		lireEndpoint.setRequestMethod("DELETE");
		lireEndpoint.setRequestProperty("Accept", "application/json");

		return call(lireEndpoint);
	}

	public List<String> similarEntries(String indexName, String sampleValue) throws IOException {
		String sampleParameter = "?search=" + sampleValue;
		URI target = getUriFromString(HOST + "/index/" + indexName + "/" + "search" + sampleParameter);
		HttpURLConnection lireEndpoint = getConnection(target);
		lireEndpoint.setDoOutput(true);
		lireEndpoint.setRequestMethod("GET");
		lireEndpoint.setRequestProperty("Accept", "application/json");

		if(!call(lireEndpoint)) {
			return null;
		}
		String rawResult = read(lireEndpoint);
		@SuppressWarnings("unchecked")
		List<String> result = (List<String>) mapper.readValue(rawResult, List.class);
		return result;
	}
	
	public boolean update(String indexName, String oldEntry, String newEntry) throws IOException {
		boolean success = removeFromIndex(indexName, oldEntry);
		if(success) {
			success &= insertIndex(indexName, newEntry);
		}
		return success;
	}

	private HttpURLConnection getConnection(URI target) throws IOException {
		return (HttpURLConnection) target.toURL().openConnection();
	}

	private static String read(HttpURLConnection connection) throws IOException {

		// read the output from the server
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuilder stringBuilder = new StringBuilder();

		String line = null;
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line + "\n");
		}

		String result = stringBuilder.toString();
		return result;
	}

	private static boolean call(HttpURLConnection connection) throws IOException {
		connection.connect();
		log.info("Web target is " + connection.getRequestMethod() + " " + connection.getURL().toString());

		final int statusCode = connection.getResponseCode();

		int[] expectedStatusCode = { 400, 404, 409 };
		if (Arrays.stream(expectedStatusCode).anyMatch((x) -> x == statusCode)) {
			return false;
		}
		if (statusCode == 200) {
			return true;
		}

		throw new RuntimeException(
				"Failed : HTTP error code  [" + connection.getResponseCode() + "] :" + connection.getResponseMessage());
	}
	
	private URI getUriFromString(String path) throws IOException {
		try {
			return new URI(path);
		} catch (URISyntaxException e) {
			throw new IOException(e);
		}
	}
}
