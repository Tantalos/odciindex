package de.unipassau.cbir;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;

public class APITest {
	
	
	@Test
	public void testIndex() throws IOException, URISyntaxException {
		LireApi lire = new LireApi("http://localhost:8090");
		
		Assert.assertNotNull(lire.indexes());
	}
	
	@Test
	public void testDelete() throws IOException, URISyntaxException {
		LireApi lire = new LireApi("http://localhost:8090");
		
		lire.deleteIndex("TestIndex");
	}

	@Test
	public void testCreate() throws IOException, URISyntaxException {
		LireApi lire = new LireApi("http://localhost:8090");
		
		lire.createIndex("TestIndex");
	}
	
	@Test
	public void testExists() throws IOException, URISyntaxException {
		LireApi lire = new LireApi("http://localhost:8090");
		
		Assert.assertNotNull(lire.exists("TestIndex"));
	}
	
	@Test
	public void testTruncate() throws IOException, URISyntaxException {
		LireApi lire = new LireApi("http://localhost:8090");
		
		lire.truncateIndex("TestIndex");
	}
	
	@Test
	public void testInsert() throws IOException, URISyntaxException {
		LireApi lire = new LireApi("http://localhost:8090");
		
		lire.insertIndex("TestIndex", "./500Image/1.jpg");
	}
	
	@Test
	public void testRemove() throws IOException, URISyntaxException {
		LireApi lire = new LireApi("http://localhost:8090");
		
		lire.removeFromIndex("TestIndex", "./500Image/1.jpg");
	}
	
	@Test
	public void testsimilarEntries() throws IOException, URISyntaxException {
		LireApi lire = new LireApi("http://localhost:8090");
		
		Assert.assertNotNull(lire.similarEntries("TestIndex", "./500Image/1.jpg"));
	}
}
