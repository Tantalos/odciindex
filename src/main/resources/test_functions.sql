DROP DIRECTORY IMG_DIR;
DROP INDEX "LIRE";
DROP TABLE "IMG_TABLE";

CREATE DIRECTORY IMG_DIR AS '/media/sf_Oracle_DB_Developer_shared/500Image/';
CREATE TABLE "IMG_TABLE" ( "COLUMN1" BFILE );
CREATE INDEX "LIRE" ON "IMG_TABLE" ("COLUMN1") INDEXTYPE IS "LIREINDEXTYPE";


INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '0.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '1.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '2.jpg') );


SELECT * FROM "IMG_TABLE";
SELECT getFileName("COLUMN1") FROM "IMG_TABLE";
SELECT ROWID, COLUMN1 FROM "IMG_TABLE";
SELECT ROWID, COLUMN1 FROM "IMG_TABLE" WHERE getFileName(COLUMN1) = '/media/sf_Oracle_DB_Developer_shared/500Image/0.jpg';

UPDATE "IMG_TABLE" SET "COLUMN1" = BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '3.jpg') WHERE getFileName(COLUMN1) = '/media/sf_Oracle_DB_Developer_shared/500Image/0.jpg';
UPDATE "IMG_TABLE" SET "COLUMN1" = BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '0.jpg') WHERE getFileName(COLUMN1) = '/media/sf_Oracle_DB_Developer_shared/500Image/3.jpg';


DELETE FROM "IMG_TABLE" WHERE getFileName(COLUMN1) = '/media/sf_Oracle_DB_Developer_shared/500Image/0.jpg';
DELETE FROM "IMG_TABLE" WHERE getFileName(COLUMN1) = '/media/sf_Oracle_DB_Developer_shared/500Image/1.jpg';
DELETE FROM "IMG_TABLE" WHERE getFileName(COLUMN1) = '/media/sf_Oracle_DB_Developer_shared/500Image/2.jpg';



INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '0.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '1.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '2.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '3.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '4.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '5.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '6.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '7.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '8.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '9.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '10.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '11.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '12.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '13.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '14.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '15.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '16.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '17.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '18.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '19.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '20.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '21.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '22.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '23.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '24.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '25.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '26.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '27.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '28.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '29.jpg') );
INSERT INTO "IMG_TABLE" ("COLUMN1") VALUES (BFILENAME('/media/sf_Oracle_DB_Developer_shared/500Image/', '30.jpg') );


SELECT SIMILARITY(COLUMN1, '/media/sf_Oracle_DB_Developer_shared/500Image/0.jpg') as sim, 
    COLUMN1 FROM IMG_TABLE WHERE similarity(COLUMN1, '/media/sf_Oracle_DB_Developer_shared/500Image/0.jpg') >= 0;

