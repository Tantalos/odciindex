DROP FUNCTION ImageSimilarity;
DROP OPERATOR SIMILARITY FORCE;
DROP TYPE ImageIndexMethods FORCE;
DROP INDEXTYPE LireIndexType FORCE;



CREATE OR REPLACE TYPE ImageIndexMethods as object (
  KEY INTEGER, 
  
   STATIC FUNCTION ODCIGetInterfaces(
        ifclist OUT SYS.ODCIObjectList)
   RETURN NUMBER,

  STATIC FUNCTION ODCIIndexCreate(ia SYS.ODCIIndexInfo, 
    parms VARCHAR2, 
    env SYS.ODCIEnv) 
    RETURN NUMBER AS LANGUAGE JAVA
    NAME 'de.unipassau.cbir.ODCIIndex.ODCIIndexCreate(oracle.ODCI.ODCIIndexInfo, java.lang.String, oracle.ODCI.ODCIEnv) return java.math.BigDecimal',

  STATIC FUNCTION ODCIIndexDrop(
    ia SYS.ODCIIndexInfo, 
    env SYS.ODCIEnv)
    RETURN NUMBER AS LANGUAGE JAVA
    NAME 'de.unipassau.cbir.ODCIIndex.ODCIIndexDrop(oracle.ODCI.ODCIIndexInfo, oracle.ODCI.ODCIEnv) return java.math.BigDecimal',

  STATIC FUNCTION ODCIIndexInsert(
    ia SYS.ODCIIndexInfo,
    rid VARCHAR2,
    newval BFILE,
    env SYS.ODCIEnv)
    RETURN NUMBER AS LANGUAGE JAVA
    NAME 'de.unipassau.cbir.ODCIIndex.ODCIIndexInsert(oracle.ODCI.ODCIIndexInfo, java.lang.String, oracle.sql.BFILE, oracle.ODCI.ODCIEnv) return java.math.BigDecimal',

  STATIC FUNCTION ODCIIndexDelete(
    ia SYS.ODCIIndexInfo,
    rid VARCHAR2,
    oldval BFILE,
    env SYS.ODCIEnv)
    RETURN NUMBER AS LANGUAGE JAVA
    NAME 'de.unipassau.cbir.ODCIIndex.ODCIIndexDelete(oracle.ODCI.ODCIIndexInfo, java.lang.String, oracle.sql.BFILE, oracle.ODCI.ODCIEnv) return java.math.BigDecimal',

  STATIC FUNCTION ODCIIndexUpdate(
    ia SYS.ODCIIndexInfo,
    rid VARCHAR2,
    oldval BFILE,
    newval BFILE,
    env SYS.ODCIEnv)
    RETURN NUMBER AS LANGUAGE JAVA
    NAME 'de.unipassau.cbir.ODCIIndex.ODCIIndexUpdate(oracle.ODCI.ODCIIndexInfo, java.lang.String, oracle.sql.BFILE, oracle.sql.BFILE, oracle.ODCI.ODCIEnv) return java.math.BigDecimal',

  STATIC FUNCTION ODCIIndexStart(
    sctx IN OUT ImageIndexMethods,
    ia SYS.ODCIIndexInfo, 
    pi SYS.ODCIPredInfo, 
    qi SYS.ODCIQueryInfo, 
    strt NUMBER,
    stop NUMBER,
    im1 IN VARCHAR2,
    env SYS.ODCIEnv)
    RETURN NUMBER AS LANGUAGE JAVA
    NAME 'de.unipassau.cbir.ODCIIndex.ODCIIndexStart(de.unipassau.cbir.ODCIIndex[], oracle.ODCI.ODCIIndexInfo, oracle.ODCI.ODCIPredInfo, oracle.ODCI.ODCIQueryInfo, java.math.BigDecimal, java.math.BigDecimal, java.lang.String, oracle.ODCI.ODCIEnv) return java.math.BigDecimal',

  MEMBER FUNCTION ODCIIndexFetch(
    self IN ImageIndexMethods,
    nrows NUMBER,
    rids OUT SYS.ODCIRidList,
    env SYS.ODCIEnv)
    RETURN NUMBER AS LANGUAGE JAVA
    NAME 'de.unipassau.cbir.ODCIIndex.ODCIIndexFetch(java.math.BigDecimal, oracle.ODCI.ODCIRidList[], oracle.ODCI.ODCIEnv) return java.math.BigDecimal',
    
  MEMBER FUNCTION ODCIIndexClose(
    self IN ImageIndexMethods,
    env SYS.ODCIEnv)
    RETURN NUMBER AS LANGUAGE JAVA
    NAME 'de.unipassau.cbir.ODCIIndex.ODCIIndexClose(oracle.ODCI.ODCIEnv) return java.math.BigDecimal'
);


/


CREATE OR REPLACE TYPE BODY ImageIndexMethods 
IS
   STATIC FUNCTION ODCIGETINTERFACES(ifclist OUT SYS.ODCIOBJECTLIST) 
       RETURN NUMBER IS
    BEGIN
        ifclist := SYS.ODCIOBJECTLIST(SYS.ODCIOBJECT('SYS','ODCIINDEX2'));
        RETURN ODCICONST.SUCCESS;
    END ODCIGETINTERFACES;
END;
/


CREATE OR REPLACE FUNCTION ImageSimilarity( IndexName IN BFILE, 
                                            Image IN VARCHAR2)
RETURN NUMBER AS
BEGIN
	RETURN 1;
END ImageSimilarity;

/

CREATE OR REPLACE OPERATOR SIMILARITY
BINDING (BFILE, VARCHAR2) RETURN NUMBER
USING ImageSimilarity;


show errors
/


CREATE OR REPLACE INDEXTYPE LireIndexType
FOR SIMILARITY(BFILE, VARCHAR2)
USING ImageIndexMethods
WITH SYSTEM MANAGED STORAGE TABLES;

show errors
/

CREATE or REPLACE FUNCTION getFileName(image IN BFILE)
RETURN VARCHAR2 AS
    file_dir      VARCHAR2(255) := NULL;
    file_name     VARCHAR2(255) := NULL;
BEGIN
    DBMS_LOB.FILEGETNAME(image, file_dir, file_name);
    DBMS_OUTPUT.PUT_LINE(file_dir);
    RETURN file_dir || file_name;
END getFileName;
/


