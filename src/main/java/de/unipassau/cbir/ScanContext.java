package de.unipassau.cbir;

import java.util.List;

import oracle.ODCI.ODCIIndexInfo;

public class ScanContext {
	private final List<String> orderedResults;
	private int cursor = 0;
	private ODCIIndexInfo info;

	
	public ScanContext(List<String> orderedResults, ODCIIndexInfo info) {
		assert orderedResults != null;
		this.orderedResults = orderedResults;
		this.info = info;
	}
	
	public List<String> getNextResults(int n) {
		int max = orderedResults.size();
		int top = cursor + n;
		int bottom = cursor;
		if(top > max) {
			top = max;
		}
		cursor = top;
		return orderedResults.subList(bottom, top);
	}
	
	public ODCIIndexInfo getIa() {
		return info;
	}
}
