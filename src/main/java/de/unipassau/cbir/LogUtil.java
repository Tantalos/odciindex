package de.unipassau.cbir;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Handler;
import java.util.logging.Logger;

public class LogUtil {

	public static Logger getRestLogger(String loggerName) {
		final Logger log = Logger.getLogger(loggerName);
		try {
			Handler restHandler = new RestLogHandler("http://localhost:8090/loginfo");
			log.addHandler(restHandler);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}

		return log;
	}
	
	public static String stackTraceToString(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		String stackTrace = sw.toString();
		return stackTrace;
	}
	
	//alias
	public static String EtoStr(Throwable t) {
		return stackTraceToString(t);
	}
	
}
