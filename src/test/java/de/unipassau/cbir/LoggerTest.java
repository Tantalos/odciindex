package de.unipassau.cbir;

import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.Logger;

import org.junit.Test;

public class LoggerTest {

	@Test
	public void test() throws IOException {
		String testMessage = "Test log message";
		
		Logger log = Logger.getLogger("Test Logger");
		Handler restHandler = new RestLogHandler("http://localhost:8090/loginfo");
		log.addHandler(restHandler);
		 
		log.info(testMessage);
//		log.info(testMessage);
	}
}
